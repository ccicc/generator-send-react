import React from 'react';
import { shallow, mount } from 'enzyme';
import TodoItem from '../../src/Modules/Todo/TodoItem';

const props = {
  todoItem: { id: '1', title: 'test', isCompleted: true },
  editorForm: { setTargetId: jest.fn() },
  deleteTodoItem: jest.fn(),
  updateTodoItem: jest.fn()
};

const setup = () => {
  const wrapper = shallow(<TodoItem {...props} />);
  return { wrapper, props };
};

const setupByMount = () => {
  const wrapper = mount(<TodoItem {...props} />);
  return { wrapper, props };
};

describe('TodoItem', () => {
  const { wrapper, props } = setup();

  it('待办项是否渲染成功', () => {
    const { wrapper } = setupByMount();
    expect(wrapper.find('.ant-list-item').exists()).toBeTruthy();
  });

  it('编辑和删除事件是否正确触发', () => {
    const { wrapper } = setupByMount();
    wrapper.find('a').first().simulate('click', props.todoItem);
    expect(props.editorForm.setTargetId).toBeCalled();
    wrapper.find('a').last().simulate('click', props.todoItem.id);
    expect(props.deleteTodoItem).toBeCalled();
    wrapper.find('.ant-list-item').simulate('dblclick', props.todoItem);
    expect(props.editorForm.setTargetId).toBeCalled();
  });
});
