import React, { FunctionComponent, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';
import AppStore from './Stores';
import AppRouter from './Modules';
import './Styles/index.scss';

const App: FunctionComponent<any> = () => {
  useEffect(() => {
    document.getElementById('pre-loading')!.remove();
  });
  return (
    <Provider appStore={AppStore.create({})}>
      <AppRouter />
    </Provider>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
