/**
 * TodoItemStore -- 待办项Store
 *
 */

import { types, Instance } from 'mobx-state-tree';

const TodoItem = types
  .model('todoItem')
  .props({
    id: types.string,
    title: types.string,
    isCompleted: types.boolean
  })
  .actions(self => ({
    // 切换完成状态
    toggleComplete(val?: boolean): void {
      if (typeof val === 'boolean') {
        self.isCompleted = val;
      } else {
        self.isCompleted = !self.isCompleted;
      }
    }
  }));

export default TodoItem;
export type TodoItemInstance = Instance<typeof TodoItem>;
