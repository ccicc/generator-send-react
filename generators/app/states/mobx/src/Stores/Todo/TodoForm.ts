/**
 * TodoFormStore -- 待办项表单输入框Store
 *
 */

import { types, Instance } from 'mobx-state-tree';
import { TodoItemInstance } from './TodoItem';

const TodoForm = types
  .model('TodoForm')
  .props({
    value: types.optional(types.string, ''),
    targetItemId: types.optional(types.maybeNull(types.string), null)
  })
  .views(self => ({
    get trimVal() {
      return self.value.trim();
    },
    get valid() {
      return this.trimVal.length > 0;
    }
  }))
  .actions(self => ({
    // 设置todoForm绑定的todoItem项
    setTargetId(todoItem: TodoItemInstance): void {
      self.targetItemId = todoItem.id;
      self.value = todoItem.title;
    },
    // 更新value
    updateVal(value: string): void {
      self.value = value;
    },
    // 重置状态
    reset(): void {
      self.value = '';
      self.targetItemId = null;
    }
  }));

export default TodoForm;
export type TodoFormInstance = Instance<typeof TodoForm>;
