/**
 * App Router
 * 
 */

import React, { FunctionComponent } from 'react';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom';
import Todo from './Todo';

export interface AppRouterProps { }

const AppRouter: FunctionComponent<AppRouterProps> = () => {
  return (
    <HashRouter basename="/">
      <Switch>
        <Redirect exact={true} from="/" to="/todo" />
        <Route path="/todo" component={Todo} />
      </Switch>
    </HashRouter>
  );
};

export default AppRouter;
