/**
 * TodoForm
 * 
 */

import React, { FunctionComponent, ChangeEvent } from 'react';
import { observer } from 'mobx-react';
import { Checkbox, Input } from 'antd';
import { TodoInstance } from '../../Stores/Todo';
import styles from './index.scss';
import { CheckboxChangeEvent } from 'antd/lib/checkbox';

export interface TodoFormProps {
  todo: TodoInstance;
}

const TodoForm: FunctionComponent<TodoFormProps> = ({ todo }) => {
  const onValueChange = (event: ChangeEvent<HTMLInputElement>) => todo.adderForm.updateVal(event.target.value);
  const onSwitchChecked = (event: CheckboxChangeEvent) => todo.switchAllCompleted(event.target.checked);
  const onPressEnter = () => todo.addTodoItem();
  return (
    <Input
      className={styles.todoForm}
      prefix={<Checkbox checked={todo.isAllCompleted} onChange={onSwitchChecked} />}
      placeholder="输入待办项"
      value={todo.adderForm.value}
      onChange={onValueChange}
      onPressEnter={onPressEnter}
    />
  );
};

export default observer(TodoForm);
