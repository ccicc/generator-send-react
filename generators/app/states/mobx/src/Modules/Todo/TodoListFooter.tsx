/**
 * TodoListFooter
 *
 */

import React, { FunctionComponent } from 'react';
import { observer } from 'mobx-react';
import classnames from 'classnames';
import { TodoFilterType } from '../../Stores/Todo';
import styles from './index.scss';

export interface TodoFooterProps {
  len: number;
  filterType: string;
  setFilterType: (filterType: TodoFilterType) => void;
  clearCompletedItems: () => void;
}

const TodoListFooter: FunctionComponent<TodoFooterProps> = ({ len, filterType, ...props }) => {
  return (
    <div className={styles.listFooter}>
      <span>待办项: {len}</span>
      <ul className={styles.todoFilters}>
        {Object.keys(TodoFilterType).map(item => (
          <li
            key={item}
            className={classnames({ [styles.selectedFilter]: item === filterType })}
            onClick={() => props.setFilterType(item as TodoFilterType)}
          >
            {item}
          </li>
        ))}
      </ul>
      <span className={styles.clearBtn} onClick={props.clearCompletedItems}>
        清除选中项
      </span>
    </div>
  );
};

export default observer(TodoListFooter);
