import React, { FunctionComponent, useEffect } from 'react';
import ReactDOM from 'react-dom';
import './Styles/index.scss';

const App: FunctionComponent<any> = () => {
  useEffect(() => {
    document.getElementById('pre-loading')!.remove();
  });
  return (
    <h1>hello,world</h1>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
