/**
 * reducers test
 *
 */

import { adderFormReducer } from '../../../src/stores/Todo/AdderForm/reducer';
import { editorFormReducer } from '../../../src/Stores/Todo/EditorForm/reducer';
import { todoListReducer } from '../../../src/Stores/Todo/TodoList/reducer';
import { todoListFooterReducer } from '../../../src/Stores/Todo/TodoListFooter/reducer';
import { crossHandleReducer } from '../../../src/Stores/Todo';

import * as adderFormTypes from '../../../src/Stores/Todo/AdderForm/types';
import * as editorFormTypes from '../../../src/Stores/Todo/EditorForm/types';
import * as todoListTypes from '../../../src/Stores/Todo/TodoList/types';
import * as todoListFooterTypes from '../../../src/Stores/Todo/TodoListFooter/types';

// adderForm reducer
describe('adderForm reducer', () => {
  it('should return initial state', () => {
    expect(adderFormReducer(undefined, {})).toEqual({ value: '' });
  });

  it('should handle CHANGE_ADDER_VAL', () => {
    expect(
      adderFormReducer(undefined, {
        type: adderFormTypes.CHANGE_ADDER_VAL,
        payload: { value: 'hello,world' }
      })
    ).toEqual({ value: 'hello,world' });
  });
});

// editorForm reducer
describe('editorForm reducer', () => {
  it('should return initial state', () => {
    expect(editorFormReducer(undefined, {})).toEqual({ value: '', targetId: '' });
  });

  it('shoudl handle CHANGE_EDITOR_VAL', () => {
    expect(
      editorFormReducer(
        { targetId: 'mock_id', value: 'test_value' },
        { type: editorFormTypes.CHANGE_EDITOR_VAL, payload: { value: 'test_val_new' } }
      )
    ).toEqual({ targetId: 'mock_id', value: 'test_val_new' });
  });

  it('should handle CHANGE_TARGET_ID', () => {
    expect(
      editorFormReducer(
        {
          targetId: 'old_mock_id',
          value: 'test'
        },
        {
          type: editorFormTypes.CHANGE_TARGET_ID,
          payload: { id: 'new_mock_id' }
        }
      )
    ).toEqual({ value: 'test', targetId: 'new_mock_id' });
  });
});

// todoList reducer
describe('todoList reducer', () => {
  let initialState = [];
  beforeEach(() => {
    initialState = [{ id: 'mock_id', title: 'test', isCompleted: false }];
  });

  it('should return initial state', () => {
    expect(todoListReducer(undefined, {})).toEqual([]);
  });

  it('should handle ADD_TODO', () => {
    expect(
      todoListReducer(undefined, {
        type: todoListTypes.ADD_TODO,
        payload: { id: 'mock_id', title: 'test', isCompleted: false }
      })
    ).toEqual([{ id: 'mock_id', title: 'test', isCompleted: false }]);
  });

  it('should handle TOGGLE_TODO', () => {
    expect(
      todoListReducer(initialState, {
        type: todoListTypes.TOGGLE_TODO,
        payload: { id: 'mock_id' }
      })
    ).toEqual([{ id: 'mock_id', title: 'test', isCompleted: true }]);
    expect(
      todoListReducer(initialState, {
        type: todoListTypes.TOGGLE_TODO,
        payload: { id: 'mock_id', isCompleted: true }
      })
    ).toEqual([{ id: 'mock_id', title: 'test', isCompleted: true }]);
    expect(todoListReducer(initialState, { type: todoListFooterTypes.TOGGLE_TODO })).toEqual(initialState);
  });

  it('should handle UPDATE_TODO', () => {
    expect(
      todoListReducer(initialState, {
        type: todoListTypes.UPDATE_TODO,
        payload: { id: 'mock_id', title: 'hello,world' }
      })
    ).toEqual([{ id: 'mock_id', title: 'hello,world', isCompleted: false }]);
  });

  it('should handle REMOVE_TODO', () => {
    expect(
      todoListReducer(initialState, {
        type: todoListTypes.REMOVE_TODO,
        payload: { id: 'mock_id' }
      })
    ).toHaveLength(0);
  });
});

// todoListFooter reducer
describe('todoListFooter reducer', () => {
  it('should return initialState', () => {
    expect(todoListFooterReducer(undefined, {})).toEqual({
      filterType: todoListFooterTypes.FilterTypes.All
    });
  });

  it('should handle FILTER_TODO_LIST', () => {
    expect(
      todoListFooterReducer(undefined, {
        type: todoListFooterTypes.FILTER_TODO_LIST,
        payload: { filterType: todoListFooterTypes.FilterTypes.Completed }
      })
    ).toEqual({ filterType: todoListFooterTypes.FilterTypes.Completed });
  });
});

// crossHandleReducer
describe('crossHandleReducer', () => {
  let initialState = {};
  beforeEach(() => {
    initialState = {
      adderForm: { value: 'test' },
      editorForm: { targetId: 'mock_id', value: 'test' },
      list: [{ id: 'test1', title: 'test1', isCompleted: false }, { id: 'test2', title: 'test2', isCompleted: true }],
      listFooter: { filterType: todoListFooterTypes.FilterTypes.All }
    };
  });

  it('should return initialState', () => {
    expect(crossHandleReducer(initialState, {})).toEqual(initialState);
  });

  it('should handle ADD_TODO', () => {
    expect(
      crossHandleReducer(initialState, {
        type: todoListTypes.ADD_TODO
      }).adderForm.value
    ).toEqual('');
  });

  it('should handle UPDATE_TODO', () => {
    expect(
      crossHandleReducer(initialState, {
        type: todoListTypes.UPDATE_TODO
      }).editorForm
    ).toEqual({ targetId: '', value: '' });
  });

  it('should handle TOGGLE_ALL_TODO', () => {
    expect(
      crossHandleReducer(initialState, {
        type: adderFormTypes.TOGGLE_ALL_TODO,
        payload: { checked: true }
      }).list
    ).toEqual([
      ...initialState.list.map(item => {
        item.isCompleted = true;
        return item;
      })
    ]);
  });

  it('should handle CHANGE_TARGET_ID', () => {
    expect(
      crossHandleReducer(
        { ...initialState, editorForm: { targetId: 'test2' } },
        {
          type: editorFormTypes.CHANGE_TARGET_ID
        }
      ).editorForm.value
    ).toEqual('test2');
  });

  it('should handle CLEAR_COMPLETED_ITEMS', () => {
    expect(
      crossHandleReducer(initialState, {
        type: todoListFooterTypes.CLEAR_COMPLETED_ITEMS
      }).list
    ).toHaveLength(1);
  });
});
