/**
 * action creators test
 *
 */

import * as adderFormActions from '../../../src/Stores/Todo/AdderForm/action';
import * as editorFormActions from '../../../src/Stores/Todo/EditorForm/action';
import * as todoListActions from '../../../src/Stores/Todo/TodoList/action';
import * as todoFooterActions from '../../../src/Stores/Todo/TodoListFooter/action';

import * as adderFormTypes from '../../../src/Stores/Todo/AdderForm/types';
import * as editorFormTypes from '../../../src/Stores/Todo/EditorForm/types';
import * as todoListTypes from '../../../src/Stores/Todo/TodoList/types';
import * as todoFooterTypes from '../../../src/Stores/Todo/TodoListFooter/types';

describe('todoApp action creators', () => {
  it('changeAdder action', () => {
    const value = 'test';
    const expectedAction = {
      type: adderFormTypes.CHANGE_ADDER_VAL,
      payload: { value }
    };
    expect(adderFormActions.changeAdder(value)).toEqual(expectedAction);
  });

  it('toggleAll action', () => {
    const checked = true;
    const expectedAction = {
      type: adderFormTypes.TOGGLE_ALL_TODO,
      payload: { checked }
    };
    expect(adderFormActions.toggleAll(checked)).toEqual(expectedAction);
  });

  it('changeEditoVal action', () => {
    const value = 'test';
    const expectedAction = {
      type: editorFormTypes.CHANGE_EDITOR_VAL,
      payload: { value }
    };
    expect(editorFormActions.changeEditorVal(value)).toEqual(expectedAction);
  });

  it('changeTargetId action', () => {
    const id = 'test';
    const expectedAction = {
      type: editorFormTypes.CHANGE_TARGET_ID,
      payload: { id }
    };
    expect(editorFormActions.changeTargetId(id)).toEqual(expectedAction);
  });

  it('addTodo action', () => {
    const title = 'test';
    const currentAction = todoListActions.addTodo(title);
    const expectedAction = {
      type: todoListTypes.ADD_TODO,
      payload: { id: currentAction.payload.id, title, isCompleted: false }
    };
    expect(currentAction).toEqual(expectedAction);
  });

  it('toggleTodo action', () => {
    const id = 'mock_id';
    const isCompleted = false;
    const expectedAction = {
      type: todoListTypes.TOGGLE_TODO,
      payload: { id, isCompleted }
    };
    expect(todoListActions.toggleTodo(id, isCompleted)).toEqual(expectedAction);
  });

  it('updateTodo action', () => {
    const id = 'mock_id';
    const title = 'test';
    const expectedAction = {
      type: todoListTypes.UPDATE_TODO,
      payload: { id, title }
    };
    expect(todoListActions.updateTodo(id, title)).toEqual(expectedAction);
  });

  it('removeTodo action', () => {
    const id = 'mock_id';
    const expectedAction = {
      type: todoListTypes.REMOVE_TODO,
      payload: { id }
    };
    expect(todoListActions.removeTodo(id)).toEqual(expectedAction);
  });

  it('setFilterType action', () => {
    const filterType = todoFooterTypes.FilterTypes.Action;
    const expectedAction = {
      type: todoFooterTypes.FILTER_TODO_LIST,
      payload: { filterType }
    };
    expect(todoFooterActions.setFilterType(filterType)).toEqual(expectedAction);
  });

  it('clearCompletedItems action', () => {
    const expectedAction = {
      type: todoFooterTypes.CLEAR_COMPLETED_ITEMS
    };
    expect(todoFooterActions.clearCompletedItems()).toEqual(expectedAction);
  });
});
