/**
 * App Router
 * 
 */

import React, { FunctionComponent } from 'react';
import { Switch, HashRouter, Redirect, Route } from 'react-router-dom';
import Todo from './Todo';

export const AppRouter: FunctionComponent<any> = () => {
  return (
    <HashRouter basename="/">
      <Switch>
        <Redirect exact={true} from="/" to="/todo" />
        <Route path="/todo" component={Todo} />
      </Switch>
    </HashRouter>
  );
};
