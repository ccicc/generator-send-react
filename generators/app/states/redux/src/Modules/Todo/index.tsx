/**
 * Todos
 *
 */

import React, { FunctionComponent } from 'react';
import { connect } from 'react-redux';
import TodoForm from './TodoForm';
import TodoList from './TodoList';
import styles from './index.scss';

export interface TodosProps {}

const Todo: FunctionComponent<TodosProps> = () => {
  return (
    <div className={styles.root}>
      <h1 className={styles.headline}>Todos</h1>
      <TodoForm />
      <TodoList />
    </div>
  );
};

export default connect()(Todo);
