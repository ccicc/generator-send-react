/**
 * App 入口文件
 */

import React, { FunctionComponent, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { configureStore } from './Stores';
import { AppRouter } from './Modules';
import './Styles/index.scss';

const appStore = configureStore();

const App: FunctionComponent<any> = () => {
  useEffect(() => {
    document.getElementById('pre-loading')!.remove();
  });
  return (
    <Provider store={appStore}>
      <AppRouter />
    </Provider>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
