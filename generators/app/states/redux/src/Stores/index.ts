/**
 * Root Store
 *
 */

import { combineReducers, createStore, Reducer, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import loggerMiddleware from 'redux-logger';
import thunkMiddleware from 'redux-thunk';

import { todoReducer, TodoReducerType } from './Todo';

const rootReducer = combineReducers({
  todo: todoReducer as Reducer<TodoReducerType>
});

export type AppState = ReturnType<typeof rootReducer>;

export const configureStore = () => {
  let middlewares = applyMiddleware(loggerMiddleware, thunkMiddleware);
  if (process.env.NODE_ENV === 'production') {
    middlewares = applyMiddleware(thunkMiddleware);
  }
  const composedEnhancers = composeWithDevTools(middlewares);
  return createStore(rootReducer, composedEnhancers);
};
