/**
 * TodoStore
 *
 */

import { combineReducers } from 'redux';
import reduceReducers, { Action } from 'reduce-reducers';

import { TOGGLE_ALL_TODO, ToggleAllAction } from './AdderForm/types';
import { ADD_TODO, UPDATE_TODO } from './TodoList/types';
import { CHANGE_TARGET_ID } from './EditorForm/types';
import { CLEAR_COMPLETED_ITEMS } from './TodoListFooter/types';

import { adderFormReducer } from './AdderForm/reducer';
import { editorFormReducer } from './EditorForm/reducer';
import { todoListReducer } from './TodoList/reducer';
import { todoListFooterReducer } from './TodoListFooter/reducer';

import Utils from '../../Common/Utils';

// 中间Reducers
const intermediateReducer = combineReducers({
  adderForm: adderFormReducer,
  editorForm: editorFormReducer,
  list: todoListReducer,
  listFooter: todoListFooterReducer
});

export type TodoReducerType = ReturnType<typeof intermediateReducer>;

// 处理交叉数据的Reducer
export const crossHandleReducer = (state: TodoReducerType, action: Action) => {
  switch (action.type) {
    case ADD_TODO:
      return {
        ...state,
        adderForm: { ...state.adderForm, value: '' }
      };
    case UPDATE_TODO:
      return {
        ...state,
        editorForm: { ...state.editorForm, targetId: '', value: '' }
      };
    case TOGGLE_ALL_TODO:
      return {
        ...state,
        list: state.list.map(item => {
          item.isCompleted = (action as ToggleAllAction).payload.checked;
          return item;
        })
      };
    case CHANGE_TARGET_ID:
      const targetItem = state.list.find(item => item.id === state.editorForm.targetId);
      return {
        ...state,
        editorForm: {
          ...state.editorForm,
          value: targetItem ? targetItem.title : ''
        }
      };
    case CLEAR_COMPLETED_ITEMS:
      const list = state.list.filter(item => !item.isCompleted);
      Utils.store('todo_list', list);
      return { ...state, list };
    default:
      return state;
  }
};

export const todoReducer = reduceReducers<TodoReducerType>(intermediateReducer, crossHandleReducer);
