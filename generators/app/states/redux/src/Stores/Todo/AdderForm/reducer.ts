/**
 * AdderForm reducer
 *
 */

import { CHANGE_ADDER_VAL, AdderForm, AdderFormActions } from './types';

const initialState: AdderForm = {
  value: ''
};

export const adderFormReducer = (state = initialState, action: AdderFormActions): AdderForm => {
  switch (action.type) {
    case CHANGE_ADDER_VAL:
      return { ...state, value: action.payload.value };
    default:
      return state;
  }
};
