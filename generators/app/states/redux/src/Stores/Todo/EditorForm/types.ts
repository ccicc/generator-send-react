/**
 * EditorFormStore types
 * 
 */

export const CHANGE_EDITOR_VAL = 'CHANGE_EDITOR_VAL';
export const CHANGE_TARGET_ID = 'CHANGE_EDITOR_ID';

export interface EditorForm {
  targetId: string;
  value: string;
}

export interface ChangeEditorValAction {
  type: typeof CHANGE_EDITOR_VAL;
  payload: {
    value: string
  };
}

export interface ChangeTargetIdAction {
  type: typeof CHANGE_TARGET_ID;
  payload: {
    id: string
  };
}

export type EditorFormActions = ChangeEditorValAction | ChangeTargetIdAction;
