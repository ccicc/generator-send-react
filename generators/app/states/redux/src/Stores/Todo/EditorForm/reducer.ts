/**
 * EditorForm Reducer
 *
 */

import { EditorForm, EditorFormActions, CHANGE_EDITOR_VAL, CHANGE_TARGET_ID } from './types';

const initialState: EditorForm = {
  value: '',
  targetId: ''
};

export const editorFormReducer = (state = initialState, action: EditorFormActions) => {
  switch (action.type) {
    case CHANGE_EDITOR_VAL:
      return { ...state, value: action.payload.value };
    case CHANGE_TARGET_ID:
      return { ...state, targetId: action.payload.id };
    default:
      return state;
  }
};
