/**
 * TodoListFooter reducer
 *
 */

import { TodoListFooter, FilterTypes, FILTER_TODO_LIST, TodoListFooterActions, SetFilterTypeAction } from './types';
import Utils from '../../../Common/Utils';

const initialState: TodoListFooter = {
  filterType: Utils.store('todo_filterType') || FilterTypes.All
};

export const todoListFooterReducer = (state = initialState, action: TodoListFooterActions) => {
  switch (action.type) {
    case FILTER_TODO_LIST:
      const filterType = (action as SetFilterTypeAction).payload.filterType;
      Utils.store('todo_filterType', filterType);
      return { ...state, filterType };
    default:
      return state;
  }
};
