'use strict';

const utils = require('./utils');
const chalk = require('chalk');

module.exports = [
  {
    type: 'input',
    name: 'appName',
    message: chalk.green('application name: '),
    default: utils.yeoman.getAppName(),
    store: true
  },
  {
    type: 'input',
    name: 'version',
    message: chalk.green('version: '),
    default: '0.0.1',
    store: true
  },
  {
    type: 'input',
    name: 'description',
    message: chalk.green('description: '),
    default: '',
    store: true
  },
  {
    type: 'input',
    name: 'entryPoint',
    message: chalk.green('entry point: '),
    default: 'src/index.ts',
    store: true
  },
  {
    type: 'input',
    name: 'author',
    message: chalk.green('author: '),
    default: '',
    store: true
  },
  {
    type: 'email',
    name: 'email',
    message: chalk.green('email: '),
    default: '',
    store: true
  },
  {
    type: 'input',
    name: 'repository',
    message: chalk.green('git repository: '),
    default: '',
    store: true
  },
  {
    type: 'list',
    name: 'pkgman',
    message: 'package management: ',
    choices: ['yarn', 'npm'],
    default: 'npm',
    store: true
  },
  {
    type: 'list',
    name: 'state',
    message: 'react state management: ',
    choices: ['default', 'mobx', 'redux'],
    default: 'none',
    store: true
  },
  {
    type: 'list',
    name: 'license',
    message: 'license',
    choices: ['MIT', 'APACHE-2.0', 'ISC', 'GPL'],
    default: 'MIT',
    store: true
  }
];
